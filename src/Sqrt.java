import java.util.Scanner;

public class Sqrt{
    public static void main(String[] args){
        Scanner kb = new Scanner(System.in);
        while(true){
        System.out.print("Please Input Your Number : ");
        int x = kb.nextInt();
        int sqrt = Sqrts(x);
        System.out.println("Your Result : "+sqrt);

        System.out.print("Your Want to Exit? (Y/N) : ");
        String exit = kb.next();
        if(exit.equals("Y")){
            System.out.println("Thank You");
            break;
            }
        }
    }

    private static int Sqrts(int x){
        int sqrt = 0;
        while(true){
            if((sqrt*sqrt)>x){
                sqrt = sqrt-1;
                return sqrt;
            }
            sqrt++;
        }
    }
}